#include <stdio.h>
#include <stdlib.h>

int		ft_factorial(int num)
{
	return (num == 1 ? 1 : num * ft_factorial(num - 1));
}

int		ft_fac_times(int num, int res)
{
	return (num == 0 ? res : ft_fac_times(num - 1, res * num));
}

int		ft_factorial_tail(int num)
{
	return (ft_fac_times(num, 1));
}

int		main(int ac, char **av)
{
	if (ac != 2)
	{
		printf("You need input 1 number\n");
		return (0);
	}
	printf("factorial: %d\n", ft_factorial(atoi(av[1])));
	printf("factorial: %d\n", ft_factorial_tail(atoi(av[1])));
	return (0);
}