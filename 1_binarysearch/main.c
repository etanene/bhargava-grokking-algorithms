#include <stdio.h>
#include <stdlib.h>

int		binary_search(int *arr, int size, int num)
{
	int		first;
	int		last;
	int		mid;
	int		count;

	first = 0;
	last = size - 1;
	count = 0;
	while (first <= last)
	{
		mid = (first + last) / 2;
		count++;
		if (arr[mid] == num)
		{
			printf("count steps: %d\n", count);
			return (mid);
		}
		if (num < arr[mid])
			last = mid - 1;
		else
			first = mid + 1;
	}
	return (-1);
}

int		main(int ac, char **av)
{
	int		*arr;
	int		*temp;
	int		index;
	int		num;

	if (ac == 1)
	{
		printf("No input numbers");
		return (0);
	}
	arr = (int*)malloc(sizeof(int) * (ac - 1));
	temp = arr;
	av++;
	while (*av)
		*temp++ = atoi(*av++);
	printf("Number to search: ");
	scanf("%d", &num);
	if ((index = binary_search(arr, ac - 1, num)) == -1)
		printf("No number in array");
	else
		printf("Index in array: %d", index);
	free(arr);
	return (0);
}