#include <stdio.h>
#include <stdlib.h>

void	ft_selection_sort(int *arr, int size)
{
	int		min;
	int		temp;

	for (int i = 0; i < size; i++)
	{
		min = i;
		for (int j = i + 1; j < size; j++)
		{
			if (arr[j] < arr[i])
				min = j;
		}
		if (min != i)
		{
			temp = arr[min];
			arr[min] = arr[i];
			arr[i] = temp;
		}
	}
}

int		main(int ac, char **av)
{
	int		*arr;
	int		*temp;

	if (ac == 1)
	{
		printf("No input numbers\n");
		return (0);
	}
	arr = (int*)malloc(sizeof(int) * (ac - 1));
	temp = arr;
	av++;
	while (*av)
		*temp++ = atoi(*av++);
	ft_selection_sort(arr, ac - 1);
	while (--ac > 1)
	{
		printf("%d ", *arr++);
	}
	printf("%d", *arr);
	return (0);
}
