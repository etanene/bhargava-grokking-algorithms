#include <stdio.h>
#include <stdlib.h>

int		binary_search(int *arr, int size, int num)
{
	int		mid;
	int		res;

	mid = size / 2;
	if (arr[mid] == num)
		return (mid);
	if (mid == size)
		return (-1);
	if (num > arr[mid])
		return ((res = binary_search(arr + mid + 1, size - (mid + 1), num)) == -1 ? -1 : res + mid + 1);
	else
		return (binary_search(arr, size - (mid + 1), num));
}

int		main(int ac, char **av)
{
	int		*arr;
	int		*temp;
	int		index;
	int		num;

	if (ac == 1)
	{
		printf("No input numbers");
		return (0);
	}
	arr = (int*)malloc(sizeof(int) * (ac - 1));
	temp = arr;
	av++;
	while (*av)
		*temp++ = atoi(*av++);
	printf("Number to search: ");
	scanf("%d", &num);
	if ((index = binary_search(arr, ac - 1, num)) == -1)
		printf("No number in array");
	else
		printf("Index in array: %d", index);
	free(arr);
	return (0);
}