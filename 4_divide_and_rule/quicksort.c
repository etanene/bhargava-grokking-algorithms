#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void	swap(void *left, void *right, size_t size)
{
	unsigned char	*pl;
	unsigned char	*pr;
	unsigned char	temp;

	pl = (unsigned char *)left;
	pr = (unsigned char *)right;
	while (size--)
	{
		temp = *pl;
		*pl = *pr;
		*pr = temp;
		pl++;
		pr++;
	}
}

void		quicksort(void *arr, size_t num, size_t size, int (*cmp)(void*, void*))
{
	void	*left;
	void	*right;
	void	*pivot;

	if (num < 2)
		return ;
	left = arr;
	right = arr + (num - 1) * size;
	pivot = arr + (num - 1) * size / 2 - (num - 1) * size / 2 % size;
	while (left < right)
	{
		while (cmp(left, pivot) < 0)
			left += size;
		while (cmp(right, pivot) > 0)
			right -= size;
		if (left > right)
			break ;
		if (left < right)
		{
			swap(left, right, size);
			if (left == pivot)
				pivot = right;
			else if (right == pivot)
				pivot = left;
		}
		left += size;
		right -= size;
	}
	if (right >= arr)
		quicksort(arr, (right - arr) / size + 1, size, cmp);
	if (left >= arr)
		quicksort(left, num - ((left - arr) / size), size, cmp);
}

int		cmpnum(void *a, void *b)
{
	return (*(int*)a - *(int*)b);
}

int		cmpstr(void	*s1, void *s2)
{
	return (strcmp((char*)s1, (char*)s2));
}

int		main(int ac, char **av)
{
	int		*arr;
	int		*temp;

	if (ac == 1)
	{
		printf("No input\n");
		return (0);
	}
	arr = (int*)malloc(sizeof(int) * (ac - 1));
	temp = arr;
	av++;
	while (*av)
		*temp++ = atoi(*av++);
	quicksort(arr, ac - 1, sizeof(*arr), cmpnum);
	while (--ac > 1)
		printf("%d ", *arr++);
	printf("%d\n", *arr);
	return (0);
}