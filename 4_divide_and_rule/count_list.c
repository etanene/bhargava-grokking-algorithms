#include <stdio.h>
#include <stdlib.h>

typedef struct		s_list
{
	void			*data;
	struct s_list	*next;
}					t_list;

t_list	*ft_lstnew(char *data)
{
	t_list	*list;

	if ((list = (t_list*)malloc(sizeof(t_list))) == NULL)
		return (NULL);
	list->data = data;
	list->next = NULL;
	return (list);
}

void	ft_lstadd(t_list **list, t_list *new)
{
	t_list	*temp;

	if (*list)
	{
		temp = *list;
		while (temp->next)
			temp = temp->next;
		temp->next = new;
	}
	else
		*list = new;
}

int		ft_count_list(t_list *list)
{
	return (list == NULL ? 0 : 1 + ft_count_list(list->next));
}

int		main(int ac, char **av)
{
	t_list	*list;

	list = NULL;
	if (ac == 1)
	{
		printf("No input data\n");
		return (0);
	}
	av++;
	while (*av)
		ft_lstadd(&list, ft_lstnew(*av++));
	printf("count list elements: %d\n", ft_count_list(list));
	return (0);
}