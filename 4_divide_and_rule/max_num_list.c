#include <stdio.h>
#include <stdlib.h>

typedef struct		s_list
{
	int				num;
	struct s_list	*next;
}					t_list;

t_list	*ft_lstnew(int num)
{
	t_list	*lst;

	if ((lst = (t_list*)malloc(sizeof(t_list))) == NULL)
		return (NULL);
	lst->num = num;
	lst->next = NULL;
	return (lst);
}

void	ft_lstadd(t_list **lst, t_list *new)
{
	t_list	*temp;

	if (*lst)
	{
		temp = *lst;
		while (temp->next)
			temp = temp->next;
		temp->next = new;
	}
	else
		*lst = new;	
}

int		ft_maxnum_list(t_list *lst)
{
	int		max;

	return (lst->next && (lst->num < (max = ft_maxnum_list(lst->next))) ? max : lst->num);
}

int		main(int ac, char **av)
{
	t_list	*lst;

	if (ac == 1)
	{
		printf("No input data\n");
		return (0);
	}
	lst = NULL;
	av++;
	while (*av)
		ft_lstadd(&lst, ft_lstnew(atoi(*av++)));
	printf("max: %d\n", ft_maxnum_list(lst));
	return (0);
}
