#include <stdio.h>
#include <stdlib.h>

int		ft_sum(int *arr, int index)
{
	return (index == 0 ? arr[index] : arr[index] + ft_sum(arr, index - 1));
}

int		main(int ac, char **av)
{
	int		*arr;
	int		*temp;

	if (ac == 1)
	{
		printf("No input numbers\n");
		return (0);
	}
	arr = (int*)malloc(sizeof(int) * (ac - 1));
	temp = arr;
	av++;
	while (*av)
		*temp++ = atoi(*av++);
	printf("sum: %d\n", ft_sum(arr, ac - 2));
	return (0);
}