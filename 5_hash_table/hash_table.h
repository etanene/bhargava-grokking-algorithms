#ifndef HASH_TABLE_H
# define HASH_TABLE_H

# include <stdio.h>
# include <string.h>
# include <stdlib.h>

# define TABLE_SIZE		128

typedef struct		s_list
{
	char			*name;
	char			*data;
	struct s_list	*next;
}					t_list;

#endif