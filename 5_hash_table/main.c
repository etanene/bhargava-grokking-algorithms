#include "hash_table.h"

void		strdel(char **str)
{
	if (str && *str)
	{
		free(*str);
		*str = NULL;
	}
}

unsigned	hashsum(char *data)
{
	unsigned	hash;

	hash = 0;
	while (*data)
		hash += *data++;
	return (hash % TABLE_SIZE);
}

void		print_usage()
{
	printf("usage: \n\tadd - add string to table\n");
	printf("\tdel - delete string from table\n");
	printf("\tfind - find string in table\n");
	printf("\tprint - print table\n");
	printf("\thelp - show commands\n");
	printf("\texit - exit program\n");
}

t_list		*search_data(t_list **hash_table, char *name, unsigned hash)
{
	t_list	*temp;

	temp = hash_table[hash];
	while (temp)
	{
		if (!strcmp(name, temp->name))
			return (temp);
		temp = temp->next;
	}
	return (NULL);
}

int			add_str_error(char *name, char *data)
{
	strdel(&name);
	strdel(&data);
	return (0);
}

int			add_str(t_list **hash_table)
{
	char		*name = NULL;
	char		*data = NULL;
	size_t		namelen = 0;
	size_t		datalen = 0;
	ssize_t		len;
	t_list		*elem;
	unsigned	hash;

	printf("input name: ");
	if ((len = getline(&name, &namelen, stdin)) == -1)
		return (add_str_error(name, data));
	name[len - 1] = '\0';
	printf("input data: ");
	if ((len = getline(&data, &datalen, stdin)) == -1)
		return (add_str_error(name, data));
	data[len - 1] = '\0';
	hash = hashsum(name);
	if ((elem = search_data(hash_table, name, hash)) == NULL)
	{
		if ((elem = (t_list*)malloc(sizeof(t_list))) == NULL)
			return (add_str_error(name, data));
		if (!(elem->name = strdup(name)))
		{
			free(elem);
			return (add_str_error(name, data));
		}
		elem->next = hash_table[hash];
		hash_table[hash] = elem;
	}
	else
		strdel(&elem->data);
	if ((elem->data = strdup(data)) == NULL)
	{
		strdel(&elem->name);
		free(elem);
		return (add_str_error(name, data));
	}
	return (1);
}

void		zero_table(t_list **hash_table)
{
	unsigned	i;

	i = 0;
	while (i < TABLE_SIZE)
		hash_table[i++] = NULL;
}

void		print_table(t_list **hash_table)
{
	unsigned	i;
	t_list		*list;

	i = 0;
	while (i < TABLE_SIZE)
	{
		list = hash_table[i];
		while (list)
		{
			printf("name: %s, data: %s\n", list->name, list->data);
			list = list->next;
		}
		i++;
	}
}

int			find_name(t_list **hash_table)
{
	char	*name = NULL;
	size_t	namelen = 0;
	ssize_t	len = 0;
	t_list	*list;

	printf("Input name: ");
	if ((len = getline(&name, &namelen, stdin)) == -1)
		return (0);
	name[len - 1] = '\0';
	list = hash_table[hashsum(name)];
	while (list)
	{
		if (!strcmp(list->name, name))
		{
			printf("name: %s, data: %s\n", list->name, list->data);
			return (1);
		}
		list = list->next;
	}
	printf("No input name in table\n");
	return (1);
}

void		del_list(t_list **begin, t_list *list)
{
	t_list	*curr;
	t_list	*prev;

	curr = *begin;
	prev = NULL;
	while (curr)
	{
		if (curr == list)
			break ;
		prev = curr;
		curr = curr->next;
	}
	if (prev)
		prev->next = curr->next;
	else
		*begin = curr->next;
	strdel(&curr->name);
	strdel(&curr->data);
	free(curr);
}

int			del_name(t_list **hash_table)
{
	char	*name = NULL;
	size_t	namelen = 0;
	ssize_t	len = 0;
	t_list	*list;
	t_list	*temp;

	printf("Input name: ");
	if ((len = getline(&name, &namelen, stdin)) == -1)
		return (0);
	name[len - 1] = '\0';
	list = hash_table[hashsum(name)];
	temp = list;
	while (temp)
	{
		if (!strcmp(temp->name, name))
		{
			del_list(&list, temp);
			return (1);
		}
		temp = temp->next;
	}
	printf("No input name in table\n");
	return (1);
}

int			main(void)
{
	t_list		*hash_table[TABLE_SIZE];
	char		*action = NULL;
	size_t		len = 0;

	zero_table(hash_table);
	print_usage();
	while (1)
	{
		if (getline(&action, &len, stdin) == -1)
		{
			strdel(&action);
			printf("Error\n");
			continue ;
		}
		if (!strcmp(action, "help\n"))
			print_usage();
		else if (!strcmp(action, "add\n"))
		{
			if (!add_str(hash_table))
				printf("Error\n");
		}
		else if (!strcmp(action, "print\n"))
			print_table(hash_table);
		else if (!strcmp(action, "find\n"))
		{
			if (!find_name(hash_table))
				printf("Error\n");
		}
		else if (!strcmp(action, "del\n"))
		{
			if (!del_name(hash_table))
				printf ("Error\n");
		}
		else if (!strcmp(action, "exit\n"))
			break ;
		else
			printf("No action, print \'help\' to usage\n");
		strdel(&action);
	}
	return (0);
}